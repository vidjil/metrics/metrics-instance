#!/usr/bin/python
# coding: utf-8
from flask import Flask, make_response
from vidjil.tools.api_vidjil import *
import os
import random
from  conf import *

app = Flask(__name__)

@app.route('/')
def entry_point():
    return 'Hello World'

def get_env_list():
    return vidjil_servers

@app.route('/metrics_test')
def metrics_test():
    content = ""
    user     = os.getenv('METRICS_USER_EMAIL')
    password = os.getenv('METRICS_USER_PASSWORD')
    
    for vidjil_server in vidjil_servers :
        url_server = vidjil_server["url"]
        certificat = vidjil_server["certificat"]

        print( "======")
        print( f"user: {user}\npwd: {password}" )
        print( f"url_server: {url_server}\ncertificat: {certificat}" )
        content += f"user: {user}<br/>pwd: {password}<br/>url_server: {url_server}<br/>certificat: {certificat}"

    return content

def cleanUrlServer(url):
    return url.replace("https://", "").replace("/vidjil/", "")

def unavailableServer(url) -> str:
    return ""

@app.route('/metrics')
def metrics():
    user     = os.getenv('METRICS_USER_EMAIL')
    password = os.getenv('METRICS_USER_PASSWORD')
    certificat = False # For the moment, don't use it in dev mode
    formated_response = ""

    for vidjil_server in vidjil_servers :
        url_server = vidjil_server["url"]
        # certificat = vidjil_server["certificat"]

        print( "======")
        print( f"user: {user}\npwd: {password}" )
        print( f"url_server: {url_server}\ncertificat: {certificat}" )
        try:
            vidjil = Vidjil(url_server, ssl=certificat)
            vidjil.login(user, password)

            metrics = vidjil.metrics()
            # print(f"{metrics=}")
            formated_response += formated_metrics(metrics,cleanUrlServer(url_server))
        except Exception as e:
            print(e)
            print(e.args)
            unavailableServer(url_server)
    response = make_response(formated_response, 200)
    response.mimetype = "text/plain"
    return response


def formated_metrics(metrics, url_server):
    formater = {
        'users_count': {"type": "counter", "description": "The count of users."},
        'group_count': {"type": "counter", "description": "The count of groups."},
        'group_count_only_test': {"type": "counter", "description": "The count of groups displayed on vidjil."},

        'set_patients_count': {"type": "counter", "description": "The count of patients sets."},
        'set_runs_count': {"type": "counter", "description": "The count of runs sets."},
        'set_generic_count': {"type": "counter", "description": "The count of generic sets."},

        'results_count': {"type": "counter", "description": "The count of results."},
        'sequence_count': {"type": "counter", "description": "The count of sequences created."},
        
        'directories_sizes':    {"type": "gauge", "description": "Size of various directories.", "function": convert_directories_sizes},
        'disk_usage':     {"type": "gauge", "description": "Statistics on main disk usage.", "function": convert_disk_usage},

        'config_analysis':  { "type": "gauge", "description": "configs_analysis.", "function": convert_configs_analysis},
        'config_analysis_by_groups':  {"type": "gauge", "description": "configs_analysis.", "function": convert_configs_analysis_by_groups},
        
        'login_count':       {"type": "gauge", "description": "login_count.",      "function": convert_login_count},
        'status_analysis':   {"type": "gauge", "description": "status_analysis.",  "function": convert_status_analysis},
        
        # 'sequence_by_user': {"type": "gauge", "description": "sequence_by_user", "function": convert_sequence_by_user}
    }

    string = f"# Information : {metrics['message']}\n"
    not_found_metrics = "# ====== Not found metrics ======\n"

    for metrics_key in formater.keys():
        if metrics_key in metrics.keys():
            if 'function' in formater[metrics_key].keys():
                string += formater[metrics_key]["function"](metrics_key, formater[metrics_key], metrics[metrics_key], url_server)
            else:
                string += f"\
# HELP vidjil_api_request_{metrics_key} {formater[metrics_key]['description']}\n\
# TYPE vidjil_api_request_{metrics_key} {formater[metrics_key]['type']}\n\
vidjil_api_request_{metrics_key}{{server=\"{url_server}\"}} {metrics[metrics_key]}\n"
        else:
                not_found_metrics += f"# KEY NOT FOUND: {metrics_key}\n"

    string += not_found_metrics
    return string

def convert_directories_sizes(metrics_key, formater, data, url_server):
    # {"size": size_directory("/mnt/result/results"), "path": "/mnt/result/results", "name": "results"}
    string = f"\
# HELP vidjil_api_request_directories_sizes {formater['description']}\n\
# TYPE vidjil_api_request_directories_sizes {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_directories_sizes\
{{name=\"{elt['name']}\", path=\"{elt['path']}\", server=\"{url_server}\"}} \
{elt['size']}\n"
    return string

def convert_disk_usage(metrics_key, formater, data, url_server):
    # {"name": "total", "size": xxx}
    string = f"\
# HELP vidjil_api_request_disk_usage {formater['description']}\n\
# TYPE vidjil_api_request_disk_usage {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_disk_usage\
{{name=\"{elt['name']}\", server=\"{url_server}\"}} \
{elt['size']}\n"
    return string

def convert_configs_analysis(metrics_key, formater, data, url_server):
    # "configs_analysis": [{
    #       "_extra": {"COUNT(`results_file`.`id`)": 12 },
    #       "config": {"name": "multi+inc+xxx", "program": "vidjil"},
    #       "results_file": {"config_id": 2 }
    #     }, ... ],
    
    # print(data)
    string = f"\
# HELP vidjil_api_request_config_analysis {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_config_analysis\
{{config_name=\"{elt['config']['name'].replace('+','_')}\", config_program=\"{elt['config']['program']}\", config_id=\"{elt['results_file']['config_id']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`results_file`.`id`)']}\n"
    return string

def convert_configs_analysis_by_groups(metrics_key, formater, data, url_server):
    # "configs_analysis": [{
    #       "_extra": {"COUNT(`results_file`.`id`)": 12 },
    #       "config": {"name": "multi+inc+xxx", "program": "vidjil"},
    #       "results_file": {"config_id": 2 }
    #     }, ... ],
    
    print(data)
    string = f"\
# HELP vidjil_api_request_config_analysis_by_groups {formater['description']}\n\
# TYPE vidjil_api_request_config_analysis_by_groups {formater['type']}\n"
    for elt in data:
        print(elt)
        string += f"\
vidjil_api_request_config_analysis_by_groups\
{{config_name=\"{elt['config']['name'].replace('+','_')}\", config_program=\"{elt['config']['program']}\", config_id=\"{elt['results_file']['config_id']}\", group=\"{elt['auth_group']['role']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`results_file`.`id`)']}\n"
    return string

def convert_login_count(metrics_key, formater, data, url_server):
    #   "login_count": [{
    #       "_extra": {"COUNT(`auth_event`.`id`)": 14 },
    #       "auth_event": {"user_id": 1 },
    #       "auth_user": {"email": "plop@plop.com"}
    #     }, ... ]
    
    # print(data)
    string = f"\
# HELP vidjil_api_request_login_count {formater['description']}\n\
# TYPE vidjil_api_request_login_count {formater['type']}\n"
    for elt in data:
        # print(elt)
        if elt['auth_user']['email'] == "metrics@vidjil.org":
            continue
        string += f"\
vidjil_api_request_login_count\
{{user_id=\"{elt['auth_event']['user_id']}\", user_email=\"{elt['auth_user']['email']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`auth_event`.`id`)']}\n"
    return string


def convert_status_analysis(metrics_key, formater, data, url_server):
    #   "status_analysis": [{
    #       "_extra": {"COUNT(`scheduler_task`.`status`)": 6 },
    #       "scheduler_task": {"status": "COMPLETED", "task_name": "process"}
    #     }, ...]
    
    string = f"\
# HELP vidjil_api_request_status_analysis {formater['description']}\n\
# TYPE vidjil_api_request_status_analysis {formater['type']}\n"
    for elt in data:
        string += f"\
vidjil_api_request_status_analysis\
{{status=\"{elt['scheduler_task']['status']}\", task_name=\"{elt['scheduler_task']['task_name']}\", server=\"{url_server}\"}} \
{elt['_extra']['COUNT(`scheduler_task`.`id`)']}\n"
    return string


# def convert_sequence_by_user(metrics_key, formater, data):
#     #   "login_count": [{
#     #       "_extra": {"COUNT(`auth_event`.`id`)": 14 },
#     #       "auth_event": {"user_id": 1 },
#     #       "auth_user": {"email": "plop@plop.com"}
#     #     }, ... ]
    
#     print(data)
#     string = f"\
# # HELP vidjil_api_sequence_by_user {formater['description']}\n\
# # TYPE vidjil_api_sequence_by_user {formater['type']}\n"
#     for elt in data:
#         print(elt)
#         if elt['auth_user']['email'] == "metrics@vidjil.org":
#             continue
#         string += f"\
# vidjil_api_sequence_by_user\
# {{user_id=\"{elt['sequence_file']['user_id']}\"}} \
# {elt['_extra']['count_sequence(`sequence_file`.`id`)']}\n"
#     return string

# def get_file_size(file_path):
#     return os.path.getsize(file_path)

# def convert_size_file(metrics_key, formater, data):
#     #   "status_analysis": [{
#     #       "_extra": {"COUNT(`scheduler_task`.`status`)": 6 },
#     #       "scheduler_task": {"status": "COMPLETED", "task_name": "process"}
#     #     }, ...]
#     string = f"\
# # HELP vidjil_api_request_size_file {formater['description']}\n\
# # TYPE vidjil_api_request_size_file {formater['type']}\n"
#     for elt in data:
#         size = get_file_size(elt['sequence_file']['data_file'])
#         string += f"\
# vidjil_api_request_size_file\
# {{file_name=\"{elt['sequence_file']['data_file']}\"}} \
# {size}\n"
#     return string

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)


