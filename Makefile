init:
	git submodule init
	git submodule update --remote
	python3 -m pip install -U pip
	python3 -m pip install --no-cache -r requirements.txt

unit: 
	#python3 -m pytest --cov=./ --cov-report html:cov.html -v -s .
	python3 -m unittest test_main.py 