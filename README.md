# Vidjil server metrics

This software allow to use `API` to ask server some metrics that will be modifiy locally and exploited by `prometheus` service.

The project is a simple flask application that use vidjil API to connect as admin to a server.
The API script is embeded to vidjil/tools; include here as a submodule.


## How to use

1. Install Docker and clone this repository.
1. First, you need to build docker image:  
  ```
  docker build . --tag vidjil/metrics-app
  ```
1. Get server certificat as described in vidjil api documentation.  
  ```bash
  SERVERNAME=localhost # adapt it to your need
  echo -n | openssl s_client -connect $SERVERNAME:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ./cert_$SERVERNAME.pem
  ```  
1. Modify some environnement values:
    * VIDJIL_USER: Admin user account to use to get metrics. Need to be created on the interogated server (metrics@vidjil.org)
    * VIDJIL_PWD: Password of the acocunt; The usage of a docker secret can be made
    * SERVICE_NAME: Name of the docker service
    * SERVER_URL: URL of server to interogate.
    * SERVER_CHAIN: Local downloaded certificate from interogated server (server chain `.pem` file)
1. Then you should just run the container:  
```
docker-compose up -d
```  
**Note:** do not forget to change port in all places(in Dockerfile, in your app and when starting container) if you are changing it.

1. After that your app should be accessible via http://127.0.0.1:5000/ and you should see "Hello World!" there:

<details> 
  ![hello world](img/hello-world.png)
  <summary>Hello world page</summary>
</details>




## Metrics getted

Metrics getted may vary from one server to another. These metrics is in Prometheus format and will be use into Grafana vidjil dashboard (see ).

Once server is launched, you should get metrics accessible at https://localhost:5000/metrics.


<details>
  ![metrics](img/metrics.png)
  <summary>Metrics page</summary>
</details>