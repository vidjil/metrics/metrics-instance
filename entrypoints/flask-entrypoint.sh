#!/bin/bash
echo -e "\e[34m===============================\e[0m"
echo -e "\e[34m=== Start service flask metrics\e[0m"
echo -e "\e[34m=== `date +'%Y/%m/%d; %H:%M'`\e[0m"; echo

echo Variables:
echo -e user     = $VIDJIL_USER
echo -e password = $VIDJIL_PWD
echo -e url_server = $SERVER_URL
echo -e certificat = $SERVER_CHAIN

echo " ======= main.py"
python main.py
