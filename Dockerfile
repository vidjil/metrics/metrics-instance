FROM python:latest
RUN pip install Flask
EXPOSE 5000

RUN mkdir -p  app/vidjil/tools/

COPY main.py app/
COPY requirements.txt app/
COPY vidjil app/vidjil

WORKDIR /app

RUN python3 -m pip install -U pip && python3 -m pip install --no-cache -r requirements.txt


RUN echo 'if [ "$ENV" = "production" ]; then prompt_color="\e[91m"; else prompt_color="\e[34m"; fi' >> ~/.bashrc; echo 'export PS1="🐳 $prompt_color$SERVICE_NAME \e[32m[ \w ]# \e[0m"' >> ~/.bashrc
